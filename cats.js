var request = require('request')

module.exports = {
  setup: function (collection) {
    return new Promise((resolve, reject) => {
      collection.countDocuments().then((count) => {
        if (count === 0) {
          this.get()
            .then((result) => { this.save(result, collection) })
            .then(function () { resolve(collection) })
            .catch(err => reject(err))
        } else {
          resolve(collection)
        }
      })
    })
  },

  get: function () {
    return new Promise((resolve, reject) => {
      request('https://latelier.co/data/cats.json', { json: true }, (err, res, body) => {
        if (err) { reject(err) }
        let cats = body.images
        resolve(cats)
      })
    })
  },

  save: function (cats, collection) {
    return new Promise((resolve, reject) => {
      collection.insertMany(cats)
        .then(function () { resolve() })
        .catch(err => reject(err))
    })
  },

  pickTwo: function (collection) {
    return new Promise((resolve, reject) => {
      collection.aggregate([{ $sample: { size: 2 } }]).toArray()
        .then(function (result) { resolve(result) })
        .catch(err => reject(err))
    })
  }
}
