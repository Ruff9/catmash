require('dotenv').config()

var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
var cats = require('./cats.js')

var app = express()

var MongoClient = require('mongodb').MongoClient
var mongoUrl = process.env.DB_URL

if (app.get('env') === 'development') { mongoUrl = 'mongodb://localhost:27017' }

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.get('/', function (req, res, next) {
  MongoClient.connect(mongoUrl, { useNewUrlParser: true }, function (err, client) {
    if (err) { return console.log(err) }
    var db = client.db(process.env.DB_NAME)
    var collection = db.collection('cats')

    cats.setup(collection)
      .then(function (collection) { return cats.pickTwo(collection) })
      .then(function (result) {
        res.render('home', { title: 'Catmash', cats: result })
      })
      .then(function () { client.close() })
      .catch(err => console.log(err))
  })
})

app.post('/', function (req, res, next) {
  MongoClient.connect(mongoUrl, { useNewUrlParser: true }, function (err, client) {
    if (err) { return console.log(err) }
    var db = client.db(process.env.DB_NAME)
    var collection = db.collection('cats')

    collection.updateOne({ id: req.body.catId }, { $inc: { score: +1 } })
    client.close()

    res.send({ redirect: '/' })
  })
})

app.get('/classement', function (req, res, next) {
  MongoClient.connect(mongoUrl, { useNewUrlParser: true }, function (err, client) {
    if (err) { return console.log(err) }
    var db = client.db(process.env.DB_NAME)
    var collection = db.collection('cats')

    collection.find({}).sort({ score: -1 }).limit(10).toArray()
      .then(function (data) {
        res.render('index', { title: 'Catmash', cats: data })
      })
      .then(function () { client.close() })
      .catch(err => console.log(err))
  })
})

module.exports = app
