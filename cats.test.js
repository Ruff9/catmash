const { MongoClient } = require('mongodb')
var nock = require('nock')
var cats = require('./cats.js')

let connection
let db
let collection

beforeEach(async () => {
  connection = await MongoClient.connect(global.__MONGO_URI__, { useNewUrlParser: true })
  db = await connection.db(global.__MONGO_DB_NAME__)
  collection = db.collection('cats')

  nock('https://latelier.co/data/cats.json')
    .get('')
    .reply(200, {
      images: [
        { 'url': 'chat1.jpg', 'id': '1' },
        { 'url': 'chat2.jpg', 'id': '2' },
        { 'url': 'chat3.jpg', 'id': '3' }
      ] })
})

afterEach(async () => {
  await collection.drop()
  await connection.close()
})

it('should load cats from API', async () => {
  expect(await collection.countDocuments()).toEqual(0)

  await cats.setup(collection)
  expect(await collection.countDocuments()).toEqual(3)
})

it('should pick two cats from base', async () => {
  await cats.setup(collection)

  let result = await cats.pickTwo(collection)
  expect(await result.length).toEqual(2)
})
