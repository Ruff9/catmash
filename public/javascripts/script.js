$(function () {
  $('.cat-picture').click(function (e) {
    let catId = $(this).data('catid')

    $.ajax({
      url: '/',
      data: { catId },
      type: 'POST',
      success: (data) => { window.location = data.redirect },
      error: console.error
    })
  })
})
